<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ComposerInstallerDeployedPackage;

/**
 * Class PackageTypesProvider.
 *
 * @api
 */
class PackageTypesProvider
{
    /**
     * @var PackageTypesProvider
     */
    private static $instance;

    /**
     * @var string[]
     */
    private $packageTypes = [];

    /**
     * PackageTypesProvider private constructor, singleton pattern.
     */
    private function __construct()
    {
    }

    /**
     * Get self instance.
     *
     * @return PackageTypesProvider
     */
    public static function getInstance(): self
    {
        if (!isset(self::$instance)) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * Reset instance.
     */
    public static function reset(): void
    {
        self::$instance = null;
    }

    /**
     * Add package type.
     *
     * @param string $packageType
     * @param string $deployPath
     */
    public function addPackageType(
        string $packageType,
        string $deployPath
    ): void {
        $this->packageTypes[$packageType] = $deployPath;
    }

    /**
     * Remove package type.
     *
     * @param string $packageType
     */
    public function removePackageType(
        string $packageType
    ): void {
        unset($this->packageTypes[$packageType]);
    }

    /**
     * Return package types.
     *
     * @return string[]
     */
    public function getPackageTypes(): array
    {
        return \array_keys($this->packageTypes);
    }

    /**
     * Return deploy paths.
     *
     * @return string[]
     */
    public function getDeployPaths(): array
    {
        return \array_values($this->packageTypes);
    }

    /**
     * Return package types and their deploy path in package_type => deploy_path format.
     *
     * @return string[]
     */
    public function getPackageTypesInfo(): array
    {
        return $this->packageTypes;
    }

    /**
     * Return deploy path for package type.
     *
     * @param string $packageType
     *
     * @return string|null
     */
    public function getDeployPathForPackageType(
        string $packageType
    ): ?string {
        return $this->getPackageTypesInfo()[$packageType] ?? null;
    }
}
