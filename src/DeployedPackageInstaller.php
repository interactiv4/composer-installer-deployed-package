<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ComposerInstallerDeployedPackage;

use Composer\Installer\InstallerInterface;
use Composer\Installer\LibraryInstaller;

/**
 * Class DeployedPackageInstaller.
 *
 * @api
 */
class DeployedPackageInstaller extends LibraryInstaller implements InstallerInterface
{
    /**
     * {@inheritdoc}
     */
    public function supports($packageType): bool
    {
        return \in_array(
            $packageType,
            $this->getSupportedPackageTypes(),
            true
        );
    }

    /**
     * Add package type.
     *
     * @return string[]
     */
    public function getSupportedPackageTypes(): array
    {
        return PackageTypesProvider::getInstance()->getPackageTypes();
    }
}
