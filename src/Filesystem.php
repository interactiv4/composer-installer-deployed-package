<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ComposerInstallerDeployedPackage;

use Symfony\Component\Filesystem\Filesystem as SymfonyFilesystem;

/**
 * Class Filesystem.
 *
 * @api
 */
class Filesystem extends SymfonyFilesystem
{
    /**
     * {@inheritdoc}
     */
    public function copy(
        $originFile,
        $targetFile,
        $overwriteFiles = false
    ): void {
        if ($this->exists($targetFile) && !$overwriteFiles) {
            return;
        }
        
        if($originFile instanceof \SplFileInfo) {
            $originFile = $originFile->getPathname();
        }
    
        if($targetFile instanceof \SplFileInfo) {
            $targetFile = $targetFile->getPathname();
        }

        parent::copy(
            $originFile,
            $targetFile,
            $overwriteFiles
        );
    }
}
