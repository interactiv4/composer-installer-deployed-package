<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ComposerInstallerDeployedPackage;

use Composer\Composer;
use Composer\DependencyResolver\Operation\InstallOperation;
use Composer\DependencyResolver\Operation\UpdateOperation;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Installer\PackageEvent;
use Composer\Installer\PackageEvents;
use Composer\IO\IOInterface;
use Composer\Package\PackageInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;

/**
 * Class Plugin.
 *
 * @api
 */
class Plugin implements PluginInterface, EventSubscriberInterface
{
    /**
     * @var IOInterface
     */
    private $io;

    /**
     * @var DeployedPackageInstaller
     */
    private $installer;

    /**
     * @var PackageDeployer
     */
    private $packageDeployer;

    /**
     * @var PackageInterface[]
     */
    private $packages = [];

    /**
     * {@inheritdoc}
     */
    public function activate(Composer $composer, IOInterface $io): void
    {
        $this->io = $io;
        $this->installer = new DeployedPackageInstaller($io, $composer);
        $this->packageDeployer = new PackageDeployer(
            $io,
            $this->installer
        );
        $composer->getInstallationManager()->addInstaller($this->installer);
    }

    /**
     * {@inheritdoc}
     */
    public function deactivate(Composer $composer, IOInterface $io): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function uninstall(Composer $composer, IOInterface $io): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            PackageEvents::POST_PACKAGE_UPDATE => [
                ['registerAndDeployPackage', 10],
            ],
            PackageEvents::POST_PACKAGE_INSTALL => [
                ['registerAndDeployPackage', 10],
            ],
            // Ensure execute deploy after other installers with priority 0
            ScriptEvents::POST_INSTALL_CMD => [
                ['deployPackages', -10],
            ],
            ScriptEvents::POST_UPDATE_CMD => [
                ['deployPackages', -10],
            ],
        ];
    }

    /**
     * @param PackageEvent $event
     */
    public function registerAndDeployPackage(PackageEvent $event): void
    {
        /** @var \Composer\DependencyResolver\Operation\OperationInterface $operation */
        $operation = $event->getOperation();

        /** @var PackageInterface $package */
        $package = false;
        if ($operation instanceof InstallOperation) {
            $package = $operation->getPackage();
        }

        if ($operation instanceof UpdateOperation) {
            $package = $operation->getTargetPackage();
        }

        if (!$package || !$this->installer->supports($package->getType())) {
            return;
        }

        $this->io->write(
            '<fg=yellow>* Registering package for deploy: ' . $package->getPrettyName() . '</fg=yellow>'
        );
        $this->packages[] = $package;

        // Deploy instantly, redeploy all at the end, avoid others overwrite our files
        $this->packageDeployer->deployPackage($package);
    }

    /**
     * @param Event $event
     */
    public function deployPackages(Event $event): void
    {
        $this->io->write('<fg=yellow>Processing ' . __FUNCTION__ . ' in ' . $event->getName() . '</fg=yellow>');
        foreach ($this->packages as $package) {
            $this->packageDeployer->deployPackage($package);
        }
    }
}
