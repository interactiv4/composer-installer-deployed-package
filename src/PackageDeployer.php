<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ComposerInstallerDeployedPackage;

use Composer\Installer\InstallerInterface;
use Composer\IO\IOInterface;
use Composer\Package\PackageInterface;
use Interactiv4\ComposerInstallerDeployedPackage\PackageDeployer\OptionsProvider;

/**
 * Class PackageDeployer.
 *
 * @api
 */
class PackageDeployer
{
    /**
     * @var IOInterface
     */
    private $io;

    /**
     * @var InstallerInterface
     */
    private $installer;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var OptionsProvider
     */
    private $optionsProvider;

    /**
     * PackageDeployer constructor.
     *
     * @param IOInterface          $io
     * @param InstallerInterface   $installer
     * @param Filesystem|null      $filesystem
     * @param OptionsProvider|null $optionsProvider
     */
    public function __construct(
        IOInterface $io,
        InstallerInterface $installer,
        Filesystem $filesystem = null,
        OptionsProvider $optionsProvider = null
    ) {
        $this->io = $io;
        $this->installer = $installer;
        $this->filesystem = $filesystem ?? new Filesystem();
        $this->optionsProvider = $optionsProvider ?? new OptionsProvider();
    }

    /**
     * @param PackageInterface $package
     *
     * @throws \Exception
     */
    public function deployPackage(PackageInterface $package): void
    {
        if (!$this->installer->supports($package->getType())) {
            $this->io->write(
                '<fg=red>* Warning: ' .
                $package->getPrettyName() .
                ' with type ' .
                $package->getType() .
                ' is not supported, skipping...</fg=red>'
            );

            return;
        }

        $targetDir = PackageTypesProvider::getInstance()->getDeployPathForPackageType($package->getType());
        if (null === $targetDir) {
            $this->io->write(
                '<fg=red>* Warning: ' .
                $package->getPrettyName() .
                ' with type ' .
                $package->getType() .
                ' has no target dir, skipping...</fg=red>'
            );

            return;
        }

        $this->io->write('<fg=yellow>* Deploying ' . $package->getPrettyName() . '...</fg=yellow>');

        foreach ($this->optionsProvider->getSourceDirs($package) as $sourceDir) {
            $sourcePath = \realpath(
                $this->installer->getInstallPath($package) . '/' . \trim(
                    $sourceDir,
                    '/'
                ) . '/'
            );
            $targetPath = \realpath(
                \rtrim(
                    $targetDir,
                    '/'
                ) .
                '/'
            );

            if (!$this->filesystem->exists($sourcePath)) {
                $this->io->write(
                    '<fg=red>* Warning: ' .
                    $package->getPrettyName() .
                    ' with type ' .
                    $package->getType() .
                    ' has no source path ' .
                    $sourcePath .
                    ', skipping...</fg=red>'
                );
                continue;
            }

            if (!$this->filesystem->exists($targetPath)) {
                $this->filesystem->mkdir($targetPath);
            }

            $this->io->write(
                'Deploying from ' . $sourcePath . ' to ' . $targetPath . '...',
                true,
                IOInterface::VERBOSE
            );

            try {
                $this->filesystem->mirror(
                    $sourcePath,
                    $targetPath,
                    null,
                    [
                        'override' => $this->optionsProvider->isOverride($package, $sourceDir),
                        'delete' => $this->optionsProvider->isDelete($package, $sourceDir),
                    ]
                );
            } catch (\Exception $e) {
                $this->io->writeError('<fg=red>* Deployment failed : ' . $e->getMessage() . '</fg=red>');
                $this->io->writeError($e->getTraceAsString());
                throw $e;
            }
        }
    }
}
