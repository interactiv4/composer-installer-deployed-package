<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ComposerInstallerDeployedPackage\PackageDeployer;

use Composer\Package\PackageInterface;

/**
 * Class PackageDeployer.
 *
 * @api
 */
class OptionsProvider
{
    /** #@+
     * @var string
     */
    const EXTRA_KEY_MAIN = 'deployed-package';

    const EXTRA_KEY_OVERRIDE = 'override';

    const EXTRA_KEY_DELETE = 'delete';

    /*#@-*/

    /** #@+
     * @var bool
     */
    const OVERRIDE_DEFAULT_VALUE = true;

    const DELETE_DEFAULT_VALUE = false;

    /*#@-*/

    /**
     * Retrieve source dirs.
     *
     * @param PackageInterface $package
     *
     * @return string[]
     */
    public function getSourceDirs(PackageInterface $package): array
    {
        return \array_keys($this->extractOptionsFromPackage($package));
    }

    /**
     * @param PackageInterface $package
     * @param string           $dir
     *
     * @return bool
     */
    public function isOverride(
        PackageInterface $package,
        string $dir
    ): bool {
        return $this->toBoolean(
            $this->extractDirOptionsFromPackage(
                $package,
                $dir
            ),
            static::EXTRA_KEY_OVERRIDE,
            static::OVERRIDE_DEFAULT_VALUE
        );
    }

    /**
     * @param PackageInterface $package
     * @param string           $dir
     *
     * @return bool
     */
    public function isDelete(
        PackageInterface $package,
        string $dir
    ): bool {
        return $this->toBoolean(
            $this->extractDirOptionsFromPackage(
                $package,
                $dir
            ),
            static::EXTRA_KEY_DELETE,
            static::DELETE_DEFAULT_VALUE
        );
    }

    /**
     * Extract options from package extra section.
     *
     * @param PackageInterface $package
     *
     * @return array
     */
    private function extractOptionsFromPackage(
        PackageInterface $package
    ): array {
        return $package->getExtra()[static::EXTRA_KEY_MAIN] ?? [];
    }

    /**
     * Extract options from package extra section.
     *
     * @param PackageInterface $package
     * @param string           $dir
     *
     * @return array
     */
    private function extractDirOptionsFromPackage(
        PackageInterface $package,
        string $dir
    ): array {
        return $this->extractOptionsFromPackage($package)[$dir] ?? [];
    }

    /**
     * @param array      $options
     * @param int|string $index
     * @param bool       $defaultValue
     *
     * @return bool
     */
    private function toBoolean(
        array $options,
        $index,
        bool $defaultValue
    ): bool {
        if (!isset($options[$index])) {
            return $defaultValue;
        }

        $value = $options[$index];

        if (\is_bool($value)) {
            return $value;
        }

        if (\is_numeric($value)) {
            return $value > 0;
        }

        if (\is_string($value)) {
            // Use default, unless something comparable is found
            $boolValue = $defaultValue;

            switch ($value) {
                case '0':
                case 'false':
                    $boolValue = false;
                    break;
                case '1':
                case 'true':
                    $boolValue = true;
                    break;
                default:
                    break;
            }

            return $boolValue;
        }

        return $defaultValue;
    }
}
