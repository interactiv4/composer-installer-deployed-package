# Interactiv4 Composer Installer Deployed Package - (Abadoned)

Description
-----------
**THIS MODULE HAS BEEN ABANDONED AND GROUPED IN THE FOLLOWING REPOSITORY:**

- (https://bitbucket.org/interactiv4/composer-installer)


Versioning
----------
This package follows semver versioning.


Compatibility
-------------
- PHP ^8.1


Installation Instructions
-------------------------
It can be installed manually using composer, by typing the following command:

`composer require interactiv4/composer-installer-deployed-package --update-with-all-dependencies`


Development Installation Instructions
-------------------------

**For development**, to install this stand-alone package, you should proceed as follows:

Run `composer install --no-dev`, so composer gets installed under vendor, and autoload is generated.

Then, **use that composer version, instead of global one**, so it can load local files under src/ and tests/:

`vendor/bin/composer install`

That should install the rest of dependencies in a proper way, so you can start working with this package.


Support
-------
Refer to [issue tracker](https://bitbucket.org/interactiv4/composer-installer-deployed-package/issues) to open an issue if needed.


Credits
---------
Supported and maintained by Interactiv4 Team.


Contribution
------------
Any contribution is highly appreciated.
The best way to contribute code is to open a [pull request on Bitbucket](https://bitbucket.org/interactiv4/composer-installer-deployed-package/pull-requests/new).


License
-------
[MIT](https://es.wikipedia.org/wiki/Licencia_MIT)


Copyright
---------
Copyright (c) 2020 Interactiv4 S.L.