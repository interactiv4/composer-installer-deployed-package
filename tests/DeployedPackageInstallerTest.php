<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\ComposerInstallerDeployedPackage\Test;

use Composer\Installer\InstallerInterface;
use Interactiv4\ComposerInstallerDeployedPackage\DeployedPackageInstaller;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class DeployedPackageInstallerTest.
 *
 * @internal
 */
class DeployedPackageInstallerTest extends TestCase
{
    /**
     * Test class DeployedPackageInstaller exists and is an instance of InstallerInterface.
     */
    public function testInstanceOf(): void
    {
        /** @var DeployedPackageInstaller|MockObject $deployedPackageInstaller */
        $deployedPackageInstaller = $this->getMockBuilder(DeployedPackageInstaller::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
        static::assertInstanceOf(InstallerInterface::class, $deployedPackageInstaller);
    }

    /**
     * Test support types.
     */
    public function testSupports(): void
    {
        /** @var DeployedPackageInstaller|MockObject $deployedPackageInstaller */
        $deployedPackageInstaller = $this->getMockBuilder(DeployedPackageInstaller::class)
            ->disableOriginalConstructor()
            ->setMethodsExcept(['supports'])
            ->getMock()
        ;

        $deployedPackageInstaller->expects(static::any())->method('getSupportedPackageTypes')->willReturn(
            [
                'type1',
                'type3',
            ]
        );

        static::assertTrue($deployedPackageInstaller->supports('type1'));
        static::assertFalse($deployedPackageInstaller->supports('type2'));
        static::assertTrue($deployedPackageInstaller->supports('type3'));
        static::assertFalse($deployedPackageInstaller->supports('type4'));
    }
}
