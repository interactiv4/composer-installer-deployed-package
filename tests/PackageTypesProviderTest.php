<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\ComposerInstallerDeployedPackage\Test;

use Interactiv4\ComposerInstallerDeployedPackage\PackageTypesProvider;
use PHPUnit\Framework\TestCase;

/**
 * Class PackageTypesProviderTest.
 *
 * @internal
 */
class PackageTypesProviderTest extends TestCase
{
    /**
     * @var PackageTypesProvider
     */
    private $packageTypesProvider;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        PackageTypesProvider::reset();
        $this->packageTypesProvider = PackageTypesProvider::getInstance();
        parent::setUp();
    }

    /**
     * Test singleton get instance.
     */
    public function testGetInstance(): void
    {
        static::assertSame($this->packageTypesProvider, PackageTypesProvider::getInstance());
    }

    /**
     * Test default package types.
     */
    public function testDefaultPackageTypes(): void
    {
        static::assertSame(
            [],
            $this->packageTypesProvider->getPackageTypes()
        );
    }

    /**
     * Test additional package types.
     */
    public function testAdditionalPackageTypes(): void
    {
        $packageType = 'magento';
        $deployPath = './magento';

        // Check add package type
        $this->packageTypesProvider->addPackageType($packageType, $deployPath);

        static::assertSame(
            [
                $packageType => $deployPath,
            ],
            $this->packageTypesProvider->getPackageTypesInfo()
        );
        static::assertSame(
            [
                $packageType,
            ],
            $this->packageTypesProvider->getPackageTypes()
        );
        static::assertSame(
            [
                $deployPath,
            ],
            $this->packageTypesProvider->getDeployPaths()
        );

        // Check empty after removal
        $this->packageTypesProvider->removePackageType($packageType);

        static::assertSame(
            [],
            $this->packageTypesProvider->getPackageTypesInfo()
        );
        static::assertSame(
            [],
            $this->packageTypesProvider->getPackageTypes()
        );
        static::assertSame(
            [],
            $this->packageTypesProvider->getDeployPaths()
        );
    }

    /**
     * Test reset.
     */
    public function testReset(): void
    {
        static::assertSame(
            [],
            $this->packageTypesProvider->getPackageTypes()
        );

        $packageType = 'magento';
        $deployPath = './magento';

        $this->packageTypesProvider->addPackageType($packageType, $deployPath);

        // Reset
        PackageTypesProvider::reset();
        static::assertNotSame($this->packageTypesProvider, PackageTypesProvider::getInstance());

        // Reassing private instance after reset
        $this->packageTypesProvider = PackageTypesProvider::getInstance();
        static::assertSame($this->packageTypesProvider, PackageTypesProvider::getInstance());

        // Expect empty package type info after reset
        static::assertSame(
            [],
            $this->packageTypesProvider->getPackageTypesInfo()
        );
        static::assertSame(
            [],
            $this->packageTypesProvider->getPackageTypes()
        );
        static::assertSame(
            [],
            $this->packageTypesProvider->getDeployPaths()
        );
    }
}
