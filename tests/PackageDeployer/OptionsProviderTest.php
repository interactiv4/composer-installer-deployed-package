<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\ComposerInstallerDeployedPackage\Test\PackageDeployer;

use Composer\Package\PackageInterface;
use Interactiv4\ComposerInstallerDeployedPackage\PackageDeployer\OptionsProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class OptionsProviderTest.
 *
 * @internal
 */
class OptionsProviderTest extends TestCase
{
    /**
     * @var OptionsProvider
     */
    private $optionsProvider;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->optionsProvider = new OptionsProvider();
        parent::setUp();
    }

    /**
     * Test default dir options.
     */
    public function testDefaultDirOptions(): void
    {
        $undefinedSourceDir = 'undefined-dir';

        /** @var PackageInterface|MockObject $package */
        $package = $this->getMockForAbstractClass(PackageInterface::class);

        static::assertTrue($this->optionsProvider->isOverride($package, $undefinedSourceDir));
        static::assertFalse($this->optionsProvider->isDelete($package, $undefinedSourceDir));
    }

    /**
     * Test custom override.
     *
     * @param $customOverride
     * @param bool $expectedOverrideResult
     * @dataProvider customOverrideDataProvider
     */
    public function testCustomOverride(
        $customOverride,
        bool $expectedOverrideResult
    ): void {
        $sourceDirOne = 'source-dir-one';
        $sourceDirTwo = 'source-dir-two';
        $sourceDirThree = 'source-dir-three';

        /** @var PackageInterface|MockObject $package */
        $package = $this->getMockForAbstractClass(PackageInterface::class);
        $package->expects(static::any())->method('getExtra')->willReturn(
            [
                OptionsProvider::EXTRA_KEY_MAIN => [
                    $sourceDirOne => [
                        OptionsProvider::EXTRA_KEY_OVERRIDE => $customOverride,
                    ],
                    $sourceDirTwo => [
                        OptionsProvider::EXTRA_KEY_OVERRIDE => true,
                    ],
                    $sourceDirThree => [
                        OptionsProvider::EXTRA_KEY_OVERRIDE => false,
                    ],
                ],
            ]
        );

        static::assertSame(
            $expectedOverrideResult,
            $this->optionsProvider->isOverride($package, $sourceDirOne)
        );

        static::assertTrue(
            $this->optionsProvider->isOverride($package, $sourceDirTwo)
        );

        static::assertFalse(
            $this->optionsProvider->isOverride($package, $sourceDirThree)
        );
    }

    /**
     * @return array
     */
    public function customOverrideDataProvider(): array
    {
        return [
            [null, true], // return default
            [false, false],
            [true, true],
            [0, false],
            [1, true],
            [0.0, false],
            [0.1, true],
            [1.1, true],
            ['false', false],
            ['0', false],
            ['true', true],
            ['1', true],
            ['other', true], // return default
        ];
    }

    /**
     * Test custom delete.
     *
     * @param $customDelete
     * @param bool $expectedDeleteResult
     * @dataProvider customDeleteDataProvider
     */
    public function testCustomDelete(
        $customDelete,
        bool $expectedDeleteResult
    ): void {
        $sourceDirOne = 'source-dir-one';
        $sourceDirTwo = 'source-dir-two';
        $sourceDirThree = 'source-dir-three';

        /** @var PackageInterface|MockObject $package */
        $package = $this->getMockForAbstractClass(PackageInterface::class);
        $package->expects(static::any())->method('getExtra')->willReturn(
            [
                OptionsProvider::EXTRA_KEY_MAIN => [
                    $sourceDirOne => [
                        OptionsProvider::EXTRA_KEY_DELETE => $customDelete,
                    ],
                    $sourceDirTwo => [
                        OptionsProvider::EXTRA_KEY_DELETE => true,
                    ],
                    $sourceDirThree => [
                        OptionsProvider::EXTRA_KEY_DELETE => false,
                    ],
                ],
            ]
        );

        static::assertSame(
            $expectedDeleteResult,
            $this->optionsProvider->isDelete($package, $sourceDirOne)
        );

        static::assertTrue(
            $this->optionsProvider->isDelete($package, $sourceDirTwo)
        );

        static::assertFalse(
            $this->optionsProvider->isDelete($package, $sourceDirThree)
        );
    }

    /**
     * @return array
     */
    public function customDeleteDataProvider(): array
    {
        return [
            [null, false], // return default
            [false, false],
            [true, true],
            [0, false],
            [1, true],
            [0.0, false],
            [0.1, true],
            [1.1, true],
            ['false', false],
            ['0', false],
            ['true', true],
            ['1', true],
            ['other', false], // return default
        ];
    }

    /**
     * Test get source dirs.
     */
    public function testGetSourceDirs(): void
    {
        $sourceDirOne = 'source-dir-one';
        $sourceDirTwo = 'source-dir-two';
        $sourceDirThree = 'source-dir-three';

        /** @var PackageInterface|MockObject $package */
        $package = $this->getMockForAbstractClass(PackageInterface::class);
        $package->expects(static::any())->method('getExtra')->willReturn(
            [
                OptionsProvider::EXTRA_KEY_MAIN => [
                    $sourceDirTwo => [
                        OptionsProvider::EXTRA_KEY_DELETE => true,
                    ],
                    $sourceDirThree => [
                        OptionsProvider::EXTRA_KEY_DELETE => false,
                    ],
                ],
            ]
        );

        static::assertNotContains($sourceDirOne, $this->optionsProvider->getSourceDirs($package));
        static::assertContains($sourceDirTwo, $this->optionsProvider->getSourceDirs($package));
        static::assertContains($sourceDirThree, $this->optionsProvider->getSourceDirs($package));
    }
}
